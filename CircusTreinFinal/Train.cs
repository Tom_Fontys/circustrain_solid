﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTreinFinal
{
   public static class Train
   {
        public static List<Animal> AllAnimals = new List<Animal>();
        public static List<Wagon> AllwWagons = new List<Wagon>();
        public static List<Animal> VegaAnimals = new List<Animal>();
        public static List<Animal> meatAnimals = new List<Animal>();
        public static void SortAnimals()
        {
            VegaAnimals.Clear();
            meatAnimals.Clear();
            foreach (Animal vlees in Train.AllAnimals)
            {
                var canAdd = (vlees.Type == Type.Carnivoor);
                if (canAdd)
                {
                    meatAnimals.Add(vlees);
                }
            }
            foreach (Animal vega in Train.AllAnimals)
            {
                var canAdd = (vega.Type == Type.Herbivoor);
                if (canAdd)
                {
                    VegaAnimals.Add(vega);
                }
            }
        }

        public static void FillWagons()
        {
            SortAnimals();
            Wagon wagon = new Wagon();
            if(meatAnimals != null)
            {
                foreach (Animal a in meatAnimals.ToList())
                {
                    wagon.AnimalCheck(a);
                    meatAnimals.Remove(a);
                    foreach (Animal b in VegaAnimals.ToList())
                    {
                        if (wagon.AnimalCheck(b))
                        {
                            VegaAnimals.Remove(b);
                        }
                    }
                    AllwWagons.Add(wagon);
                    wagon = new Wagon();
                }
            }
            for (int i = 0; i < VegaAnimals.Count; i++)
            {
                wagon = new Wagon();
                PlantWagons(wagon);
            }
        }
        
        public static void PlantWagons(Wagon agon)
        {
          
            VegaAnimals.Sort((x, b) => b.Points.CompareTo(x.Points));
            foreach (Animal b in VegaAnimals.ToList())
            {
               if (agon.AnimalCheck(b))
               {
                    VegaAnimals.Remove(b);
               }
            }
            AllwWagons.Add(agon);
            agon = new Wagon();
        }
    }
}
