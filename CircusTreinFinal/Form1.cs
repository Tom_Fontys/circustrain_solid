﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CircusTreinFinal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBox1.DataSource = Enum.GetValues(typeof(Size));
            comboBox2.DataSource = Enum.GetValues(typeof(Type));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Type type = (Type)Enum.Parse(typeof(Type), comboBox2.SelectedValue.ToString());
            Size size = (Size)Enum.Parse(typeof(Size), comboBox1.SelectedValue.ToString());
            Animal animal = new Animal(size, type);

            Train.AllAnimals.Add(animal);
            foreach (var r in Train.AllAnimals)
            {
                listBox1.Items.Add(r);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Train.FillWagons();
            foreach (Wagon w in Train.AllwWagons)
            {
                listBox2.Items.Add(w);
            }
        }
    }
}
