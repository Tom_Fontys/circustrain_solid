﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTreinFinal
{
    public enum Size
    {
        Small,
        Medium, Tall
    }
    public enum Type { Carnivoor, Herbivoor }
    public class Animal
    {
        public int Points { get; }

        //public Size Size{ get { return size; } }
        public Type Type { get { return type; } }
        private Type type;
        private Size size;
        public Animal(Size Size, Type Type)
        {
            size = Size;
            type = Type;
            if (Size == Size.Small)
                Points = 1;
            if (Size == Size.Medium)
                Points = 3;
            if (Size == Size.Tall)
                Points = 5;
        }
        public Animal(Type Type)
        {

        }
        public override string ToString()
        {
            return type.ToString() + " " + Points.ToString();
        }
    }
}
