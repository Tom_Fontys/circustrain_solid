﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTreinFinal
{
    public class Wagon
    {
        private int MaxPoints = 10;
        public int Points
        {
            get { return points; }
            set { points = value; }
        }

        private int points;
        public List<Animal> Animals
        {
            get { return animals; }
            set
            {
                animals = value;
            }
        }
        private List<Animal> animals = new List<Animal>();

       private bool CanAddAnimal(Animal a)
       {
            if (this.points + a.Points > MaxPoints)
            {
                return false;
            }
            bool contains = animals.Any(p => p.Type == Type.Carnivoor);
            if (contains && a.Type == Type.Carnivoor)
            {
                return false;
            }
            if (a.Type == Type.Herbivoor && animals.Any(x => x.Type == Type.Carnivoor && x.Points >= a.Points))
            {
                return false;
            }
                return true; 
       }

        public bool AnimalCheck(Animal a)
        {
            if (CanAddAnimal(a))
            {
                AddAnimal(a);
                return true;
            }
            return false;
        }
      
       private void AddAnimal(Animal a)
       {
           animals.Add(a);
           points = points + a.Points;
       }
        public override string ToString()
        {
            return "Wagon:" + String.Concat(animals) + " || " + points.ToString();
        }
    }
}
