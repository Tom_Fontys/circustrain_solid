﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CircusTreinFinal;
using System.Collections.Generic;

namespace CircusTreinFinal.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void SlackMethod1()
        {
            //arrange
            Animal Animal = new Animal(Size.Small, Type.Herbivoor);
            Animal Animal1 = new Animal(Size.Small, Type.Herbivoor);
            Animal animal1 = new Animal(Size.Small, Type.Herbivoor);
            Animal animal2 = new Animal(Size.Small, Type.Herbivoor);
            Animal animal3 = new Animal(Size.Small, Type.Herbivoor);
            Animal animal4 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal5 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal6 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal7 = new Animal(Size.Tall, Type.Herbivoor);

           
            Train.AllAnimals.Add(Animal);
            Train.AllAnimals.Add(Animal1);
            Train.AllAnimals.Add(animal1);
            Train.AllAnimals.Add(animal2);
            Train.AllAnimals.Add(animal3);
            Train.AllAnimals.Add(animal4);
            Train.AllAnimals.Add(animal5);
            Train.AllAnimals.Add(animal6);
            Train.AllAnimals.Add(animal7);

            Wagon wagon = new Wagon();
            Wagon wagon2 = new Wagon();
            List<Wagon> AllwWagons = new List<Wagon>();
            AllwWagons.Add(wagon);
            AllwWagons.Add(wagon2);
            //act

            Train.FillWagons();

            //assert
            Assert.AreEqual(AllwWagons.Count, Train.AllwWagons.Count);

        }
        [TestMethod]
        public void SlackMethod5()
        {
            //arrange
            Animal Animal = new Animal(Size.Small, Type.Herbivoor);
           
            Animal animal4 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal5 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal6 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal7 = new Animal(Size.Tall, Type.Herbivoor);
            Animal animal8 = new Animal(Size.Tall, Type.Herbivoor);

            Train.AllAnimals.Add(Animal);
            Train.AllAnimals.Add(animal8);
           
            Train.AllAnimals.Add(animal4);
            Train.AllAnimals.Add(animal5);
            Train.AllAnimals.Add(animal6);
            Train.AllAnimals.Add(animal7);

            Wagon wagon = new Wagon();
            Wagon wagon2 = new Wagon();
            List<Wagon> AllwWagons = new List<Wagon>();
            AllwWagons.Add(wagon);
            AllwWagons.Add(wagon2);
            //act
            Train.AllAnimals.Clear();
            Train.AllwWagons.Clear();

            Train.AllAnimals.Add(Animal);
            Train.AllAnimals.Add(animal8);

            Train.AllAnimals.Add(animal4);
            Train.AllAnimals.Add(animal5);
            Train.AllAnimals.Add(animal6);
            Train.AllAnimals.Add(animal7);

            Train.FillWagons();

            //assert
            Assert.AreEqual(AllwWagons.Count, Train.AllwWagons.Count);

        }
        [TestMethod]
        public void SlackMethod6()
        {
            //arrange
            Animal Animal = new Animal(Size.Small, Type.Carnivoor);

            Animal animal4 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal5 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal6 = new Animal(Size.Medium, Type.Herbivoor);
            Animal animal7 = new Animal(Size.Tall, Type.Herbivoor);
            Animal animal8 = new Animal(Size.Tall, Type.Herbivoor);

            Train.AllAnimals.Add(Animal);
            Train.AllAnimals.Add(animal8);

            Train.AllAnimals.Add(animal4);
            Train.AllAnimals.Add(animal5);
            Train.AllAnimals.Add(animal6);
            Train.AllAnimals.Add(animal7);

            Wagon wagon = new Wagon();
            Wagon wagon2 = new Wagon();
            List<Wagon> AllwWagons = new List<Wagon>();
            AllwWagons.Add(wagon);
            AllwWagons.Add(wagon2);
            //act
            Train.AllAnimals.Clear();
            Train.AllwWagons.Clear();
            Train.AllAnimals.Add(Animal);
            Train.AllAnimals.Add(animal8);

            Train.AllAnimals.Add(animal4);
            Train.AllAnimals.Add(animal5);
            Train.AllAnimals.Add(animal6);
            Train.AllAnimals.Add(animal7);
            Train.FillWagons();

            //assert
            Assert.AreEqual(AllwWagons.Count, Train.AllwWagons.Count);

        }
    }

}
